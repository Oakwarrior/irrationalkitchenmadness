﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderItem : MonoBehaviour
{
    public OrderItemFoodStatus FoodStatusTemplate;
    public Image IconTimerClock;
    public Image IconTimerStatus;

    public Sprite SpriteTimerStatusOK;
    public Sprite SpriteTimerStatusNotOK;
    public Sprite SpriteTimerStatusReallyNotOK;

    Order AssignedOrder;

    List<OrderItemFoodStatus> FoodStatusList = new List<OrderItemFoodStatus>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float progress = Mathf.Clamp(AssignedOrder.GetOrderTimeLeftProgress(), 0.0f, 1.0f);
        IconTimerClock.fillAmount = progress;
        switch (AssignedOrder.GetOrderStatus())
        {
            case EOrderStatus.VALID:
                IconTimerStatus.sprite = SpriteTimerStatusOK;
                break;
            case EOrderStatus.LATE:
                IconTimerStatus.sprite = SpriteTimerStatusNotOK;
                break;
            case EOrderStatus.REALLY_LATE:
                IconTimerStatus.sprite = SpriteTimerStatusReallyNotOK;
                break;
        }
    }

    public void InitOrderItem(Order order)
    {
        AssignedOrder = order;

        for (int i = 0; i < order.FoodList.Count; ++i)
        {
            OrderItemFoodStatus newStatus = Instantiate(FoodStatusTemplate);
            newStatus.InitFoodStatus(order.FoodList[i]);
            newStatus.transform.SetParent(gameObject.transform);
        }
    }
}
