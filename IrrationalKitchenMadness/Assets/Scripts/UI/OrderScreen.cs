﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderScreen : MonoBehaviour
{
    public static OrderScreen instance;

    private void Awake()
    {
        instance = this;
    }

    public OrderItem OrderItemTemplate;
    public Canvas CanvasOrder;

    Dictionary<Order, OrderItem> OrderItems = new Dictionary<Order, OrderItem>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NotifyOrderGenerated(Order order)
    {
        OrderItem newItem = Instantiate(OrderItemTemplate);
        newItem.InitOrderItem(order);
        newItem.transform.SetParent(CanvasOrder.gameObject.transform);
        newItem.transform.localRotation = new Quaternion();
        newItem.transform.localPosition = new Vector3();
        OrderItems.Add(order, newItem);
    }

    public void NotifyOrderDone(Order order)
    {
        Destroy(OrderItems[order].gameObject);
        OrderItems.Remove(order);
    }
}
