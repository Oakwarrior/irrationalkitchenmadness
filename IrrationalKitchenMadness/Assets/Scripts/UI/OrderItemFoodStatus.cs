﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderItemFoodStatus : MonoBehaviour
{

    public Image IconFood;
    public Image IconFoodStatus;

    public Sprite SpriteFoodStatusAvailable;
    public Sprite SpriteFoodStatusUnavailable;

    Food AssignedFood;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(FoodConsumer.instance.CheckFoodAvailability(AssignedFood))
        {
            IconFoodStatus.sprite = SpriteFoodStatusAvailable;
        }
        else
        {
            IconFoodStatus.sprite = SpriteFoodStatusUnavailable;
        }
    }

    public void InitFoodStatus(Food food)
    {
        AssignedFood = food;
        IconFood.sprite = food.FoodIcon;
    }
}
