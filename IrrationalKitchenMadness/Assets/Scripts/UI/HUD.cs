﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public static HUD instance;

    private void Awake()
    {
        instance = this;
    }

    public string GameSceneName = "";

    public Text TextMoney;
    public Text TextEnd;
    public Text TextSurvivalTime;
    public Button ButtonRestart;
    public Text TextLookTarget;

    // Start is called before the first frame update
    void Start()
    {
        ButtonRestart.onClick.AddListener(OnRestartClicked);
        ButtonRestart.gameObject.SetActive(false);
        TextEnd.gameObject.SetActive(false);
        TextSurvivalTime.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateLookTargetText(string text)
    {
        TextLookTarget.text = text;
    }

    public void UpdateMoney()
    {
        TextMoney.text = KitchenManager.instance.GetMoney().ToString();
    }

    public void NotifyWin(float survivalTime)
    {
        TextEnd.text = "YOU WON!";
        TextEnd.gameObject.SetActive(true);
        ButtonRestart.gameObject.SetActive(true);
        TextSurvivalTime.gameObject.SetActive(true);
        TextSurvivalTime.text = "YOU SURVIVED " + survivalTime.ToString("F2") + "s";
    }

    public void NotifyLose(float survivalTime)
    {
        TextEnd.text = "YOU LOST!";
        TextEnd.gameObject.SetActive(true);
        ButtonRestart.gameObject.SetActive(true);
        TextSurvivalTime.gameObject.SetActive(true);
        TextSurvivalTime.text = "YOU SURVIVED " + survivalTime.ToString("F2") + "s";
    }

    public void OnRestartClicked()
    {
        SceneManager.LoadScene(GameSceneName);
    }
}
