﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : ManagerBase
{
    public static InputManager instance;

    private void Awake()
    {
        instance = this;
    }

    public float TurnSensitivity = 5f;
    public float LookSensitivity = 5f;



    // Update is called once per frame
    void Update()
    {
        HandleInput();
    }


    void HandleInput()
    {
        if(KitchenManager.instance.HasGameEnded())
        {
            return;
        }

        float rotationX = Input.GetAxis("Mouse X") * TurnSensitivity;
        float rotationY = Input.GetAxis("Mouse Y") * LookSensitivity;


        CharacterPlayer.instance.HandleTurn(rotationX);
        CharacterPlayer.instance.HandleLook(rotationY);

        Vector3 moveVector = new Vector3();

        if (Input.GetKey(KeyCode.W))
        {
            moveVector += CharacterPlayer.instance.gameObject.transform.forward;
        }
        if (Input.GetKey(KeyCode.S))
        {
            moveVector -= CharacterPlayer.instance.gameObject.transform.forward;
        }
        if (Input.GetKey(KeyCode.A))
        {
            moveVector -= CharacterPlayer.instance.gameObject.transform.right;
        }
        if (Input.GetKey(KeyCode.D))
        {
            moveVector += CharacterPlayer.instance.gameObject.transform.right;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            CharacterPlayer.instance.HandleInteract();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            CharacterPlayer.instance.DropCarriedFood();
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        CharacterPlayer.instance.HandleMove(moveVector.normalized);
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }
}
