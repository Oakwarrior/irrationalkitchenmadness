﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class OrderManager : ManagerBase
{
    public static OrderManager instance;

    private void Awake()
    {
        instance = this;
    }

    public int MaxOrderTime = 35;
    public int MinOrderTime = 25;

    public float OrderSuccessMultiplier = 2.0f;
    public float OrderLateMultiplier = 1.0f;
    public float OrderReallyLateMultiplier = 0.5f;

    public float OrderInterval = 15.0f;

    float OrderTimer = 0.0f;

    List<Order> OrderList = new List<Order>();

    public override void InitManager()
    {
        base.InitManager();

        OrderTimer = 1.0f;
    }

    private void Update()
    {
        OrderTimer -= Time.deltaTime;
        if (OrderTimer <= 0.0f)
        {
            GenerateOrder();
            OrderTimer = OrderInterval;
        }

        TickOrders();
    }

    private void TickOrders()
    {
        for (int i = 0; i < OrderList.Count; ++i)
        {
            OrderList[i].TickOrder(Time.deltaTime);
        }
    }

    public void GenerateOrder()
    {
        int numOfIngredients = Random.Range(2, 5);

        List<Food> foodList = new List<Food>(FoodManager.instance.FoodList);

        List<Food> randomFoodList = new List<Food>();
        for (int i = 0; i < numOfIngredients; ++i)
        {
            int randomIndex = Random.Range(0, foodList.Count);
            Food randomFood = foodList[randomIndex];
            foodList.RemoveAt(randomIndex);
            randomFoodList.Add(randomFood);

        }

        Order newOrder = new Order(randomFoodList, Random.Range(MinOrderTime, MaxOrderTime + 1));
        OrderList.Add(newOrder);

        FoodManager.instance.NotifyOrderGenerated(newOrder);
        OrderScreen.instance.NotifyOrderGenerated(newOrder);
    }

    public void NotifyOrderDone(Order order)
    {
        OrderList.Remove(order);

        float cost = 0;
        float profit = 0;

        for (int i = 0; i < order.FoodList.Count; ++i)
        {
            cost += order.FoodList[i].Price;
            profit += order.FoodList[i].Price;
        }

        switch (order.GetOrderStatus())
        {
            case EOrderStatus.VALID:
                profit *= OrderSuccessMultiplier;
                break;
            case EOrderStatus.LATE:
                profit *= OrderLateMultiplier;
                break;
            case EOrderStatus.REALLY_LATE:
                profit *= OrderReallyLateMultiplier;
                break;
        }

        KitchenManager.instance.ModifyMoney(Mathf.RoundToInt(profit - cost));
    }

    public List<Order> GetOrders()
    {
        return OrderList;
    }

}
