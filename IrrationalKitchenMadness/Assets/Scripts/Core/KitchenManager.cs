﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenManager : ManagerBase
{
    public static KitchenManager instance;

    private void Awake()
    {
        instance = this;
    }

    public int StartingMoney = 100;
    public int LoseThreshold = 0;
    public int WinThreshold = 200;

    float SurvivalTime = 0.0f;

    int Money = 100;

    bool bHasGameEnded = false;

    public override void InitManager()
    {
        base.InitManager();

        Money = StartingMoney;
        HUD.instance.UpdateMoney();
    }

    private void Update()
    {
        if(!bHasGameEnded)
        {
            SurvivalTime += Time.deltaTime;
        }
    }

    public void ModifyMoney(int amount)
    {
        Money += amount;

        if(Money <= LoseThreshold)
        {
            LoseGame();
        }
        if(Money >= WinThreshold)
        {
            WinGame();
        }

        HUD.instance.UpdateMoney();
    }

    public void WinGame()
    {
        bHasGameEnded = true;
        HUD.instance.NotifyWin(SurvivalTime);
    }

    public void LoseGame()
    {
        bHasGameEnded = true;
        HUD.instance.NotifyLose(SurvivalTime);
    }

    public int GetMoney()
    {
        return Money;
    }

    public bool HasGameEnded()
    {
        return bHasGameEnded;
    }
}
