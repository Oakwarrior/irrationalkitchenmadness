﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FoodManager : ManagerBase
{
    public static FoodManager instance;

    private void Awake()
    {
        instance = this;
    }

    public Vector3 FoodContainerSpawnPosition;
    public FoodContainer FoodContainerTemplate;
    public List<Food> FoodList = new List<Food>();

    Dictionary<Food, FoodContainer> FoodContainers = new Dictionary<Food, FoodContainer>();

    public override void InitManager()
    {
        base.InitManager();

        SpawnFoodContainers();
    }


    private void SpawnFoodContainers()
    {
        for (int i = 0; i < FoodList.Count; ++i)
        {
            FoodContainer newContainer = Instantiate(FoodContainerTemplate);
            newContainer.InitFoodContainer(FoodList[i]);
            newContainer.transform.position = FoodContainerSpawnPosition + new Vector3(3 * i, 0, 0);
            FoodContainers.Add(FoodList[i], newContainer);
        }
    }

    public void NotifyOrderGenerated(Order order)
    {
        for (int i = 0; i < order.FoodList.Count; ++i)
        {
            FoodContainers[order.FoodList[i]].SpawnFood();
        }
    }
}
