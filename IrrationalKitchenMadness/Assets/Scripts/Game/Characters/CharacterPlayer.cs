﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPlayer : CharacterBase
{
    public static CharacterPlayer instance;

    private void Awake()
    {
        instance = this;
    }

    Quaternion originalRotation;

    public GameObject CarrySlot;
    public float LookAngleMax = 60;
    public float LookAngleMin = -60;

    float LookRotation = 0.0f;

    FoodItem CarriedFood;

    // Start is called before the first frame update
    void Start()
    {
        originalRotation = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 10))
        {
            FoodItem foodItem = hit.collider.gameObject.GetComponent<FoodItem>();
            FoodConsumer foodConsumer = hit.collider.gameObject.GetComponent<FoodConsumer>();
            FoodContainer foodContainer = hit.collider.gameObject.GetComponent<FoodContainer>();

            if(foodItem != null)
            {
                HUD.instance.UpdateLookTargetText(foodItem.GetFoodData().Name);
            }
            else if (foodConsumer != null)
            {
                HUD.instance.UpdateLookTargetText("Food Dispenser");
            }
            else if (foodContainer != null)
            {
                HUD.instance.UpdateLookTargetText(foodContainer.GetSpawnedFoodName() + " Container ");
            }
            else
            {
                HUD.instance.UpdateLookTargetText("");
            }
        }
    }

    public void HandleLook(float rotation)
    {
        LookRotation += rotation;
        LookRotation = InputManager.ClampAngle(LookRotation, LookAngleMin, LookAngleMax);

        Quaternion yQuaternion = Quaternion.AngleAxis(LookRotation, Vector3.left);

        Camera.main.gameObject.transform.localRotation = originalRotation * yQuaternion;
    }

    public FoodItem GetCarriedFood()
    {
        return CarriedFood;
    }

    public void HandleInteract()
    {
        
        RaycastHit hit;
        int triggerMask = 1 << 9;
        triggerMask = ~triggerMask;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 10, triggerMask))
        {
            MonoBehaviour[] componentList = hit.collider.gameObject.GetComponents<MonoBehaviour>();
            Interactable interactable = null;
            for(int i = 0; i < componentList.Length; ++i)
            {
                if (componentList[i] is Interactable)
                {
                    interactable = (Interactable)componentList[i];
                    break;
                }
            }

            Debug.Log("Try interact: " + hit.collider.gameObject);
            if(interactable != null)
            {
                interactable.Interact(this);
            }
        }
    }

    public void SetCarriedFood(FoodItem food)
    {
        if(CarriedFood == null)
        {
            CarriedFood = food;
            CarriedFood.HandlePickup(CarrySlot);
        }
    }

    public void DropCarriedFood()
    {
        if(CarriedFood != null)
        {
            CarriedFood.HandleDrop();
            CarriedFood = null;
        }

    }

}
