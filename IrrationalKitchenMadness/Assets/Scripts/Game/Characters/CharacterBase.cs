﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBase : MonoBehaviour
{

    public Rigidbody CharacterRigidBody;
    public float MoveSpeed = 1.0f;


    public virtual void HandleMove(Vector3 moveVector)
    {
        moveVector = gameObject.transform.position + new Vector3(moveVector.x * MoveSpeed, moveVector.y, moveVector.z * MoveSpeed) * Time.deltaTime;
        CharacterRigidBody.MovePosition(moveVector);

    }

    public void HandleTurn(float rotation)
    {
        Quaternion xQuaternion = Quaternion.AngleAxis(rotation, Vector3.up);

        CharacterRigidBody.MoveRotation(gameObject.transform.localRotation * xQuaternion);

    }
}
