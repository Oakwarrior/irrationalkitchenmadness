﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodItem : MonoBehaviour, Interactable
{
    Food FoodData;

    public Rigidbody FoodRigidBody;
    public Collider FoodCollider;

    GameObject CarrySlot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(CarrySlot != null)
        {
            FoodRigidBody.MovePosition(CarrySlot.transform.position);
        }
    }

    public void InitFood(Food data)
    {
        FoodData = data;
    }

    public Food GetFoodData()
    {
        return FoodData;
    }

    public Vector3 GetPosition()
    {
        return gameObject.transform.position;
    }

    public void Interact(CharacterBase interactor)
    {
        CharacterPlayer player = (CharacterPlayer)interactor;
        if(player != null)
        {
            player.SetCarriedFood(this);
        }
    }

    public void HandlePickup(GameObject carrySlot)
    {
        gameObject.transform.SetParent(carrySlot.transform);
        gameObject.transform.position = carrySlot.transform.position;
        FoodRigidBody.useGravity = false;
        CarrySlot = carrySlot;
    }
    
    public void HandleDrop()
    {
        FoodRigidBody.useGravity = true;
        gameObject.transform.SetParent(null);
        CarrySlot = null;
    }
}
