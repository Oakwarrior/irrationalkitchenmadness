﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodConsumer : MonoBehaviour, Interactable
{
    public static FoodConsumer instance;

    private void Awake()
    {
        instance = this;
    }

    public Collider ConsumerCollider;

    List<FoodItem> StoredFoodList = new List<FoodItem>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckOrderCompletion();
    }

    public Vector3 GetPosition()
    {
        return gameObject.transform.position;
    }

    public void Interact(CharacterBase interactor)
    {

    }

    public void AddFood(FoodItem item)
    {
        StoredFoodList.Add(item);
        Debug.Log("Added food: " + item.gameObject);
    }

    public void RemoveFood(FoodItem item, bool destroy = false)
    {
        StoredFoodList.Remove(item);
        if(destroy)
        {
            Destroy(item.gameObject);
        }
        Debug.Log("Removed food: " + item.gameObject);
    }

    public void CheckOrderCompletion()
    {
        List<Order> orders = OrderManager.instance.GetOrders();

        for (int i = 0; i < orders.Count; ++i)
        {
            bool orderMatch = true;
            for (int j = 0; j < orders[i].FoodList.Count; ++j)
            {
                if (!CheckFoodAvailability(orders[i].FoodList[j]))
                {
                    orderMatch = false;
                    break;
                }
            }
            if (orderMatch)
            {
                CompleteOrder(orders[i]);
                break;
            }
        }
    }

    public bool CheckFoodAvailability(Food food)
    {
        for (int i = 0; i < StoredFoodList.Count; ++i)
        {
            if (StoredFoodList[i].GetFoodData() == food)
            {
                return true;
            }
        }

        return false;
    }

    public void CompleteOrder(Order order)
    {
        for(int i = 0; i < order.FoodList.Count; ++i)
        {
            ConsumeFoodItem(order.FoodList[i]);
        }

        OrderManager.instance.NotifyOrderDone(order);
        OrderScreen.instance.NotifyOrderDone(order);
        // money handling
    }

    public void ConsumeFoodItem(Food food)
    {
        for (int i = StoredFoodList.Count - 1; i >= 0; --i)
        {
            if (StoredFoodList[i].GetFoodData() == food)
            {
                Destroy(StoredFoodList[i].gameObject);
                StoredFoodList.RemoveAt(i);
                return;
            }
        }

        Debug.LogError("Fix your stuff");
    }

    private void OnTriggerEnter(Collider other)
    {
        FoodItem foodItem = other.gameObject.GetComponent<FoodItem>();

        if(foodItem != null)
        {
            AddFood(foodItem);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        FoodItem foodItem = other.gameObject.GetComponent<FoodItem>();

        if (foodItem != null)
        {
            RemoveFood(foodItem);
        }
    }
}
