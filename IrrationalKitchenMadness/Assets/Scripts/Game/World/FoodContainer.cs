﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodContainer : MonoBehaviour
{
    Food SpawnedFood;

    public GameObject SpawnMarker;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitFoodContainer(Food spawnedFood)
    {
        SpawnedFood = spawnedFood;
    }

    public void SpawnFood()
    {
        FoodItem spawnedFood = Instantiate(SpawnedFood.FoodItemTemplate);
        spawnedFood.gameObject.transform.position = SpawnMarker.gameObject.transform.position + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));
        spawnedFood.gameObject.transform.rotation = Random.rotation;
        spawnedFood.InitFood(SpawnedFood);


    }

    public string GetSpawnedFoodName()
    {
        return SpawnedFood.Name;
    }
}
