﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EOrderStatus
{
    VALID,
    LATE,
    REALLY_LATE,
}

[System.Serializable]
public class Order
{
    public List<Food> FoodList = new List<Food>();

    float InitialTime = 0;
    float RemainingTime = 0;

    public Order()
    {

    }

    public Order(List<Food> foodList, int orderTime)
    {
        FoodList = foodList;
        RemainingTime = InitialTime = orderTime;
    }

    public void TickOrder(float deltaTime)
    {
        RemainingTime -= deltaTime;
    }

    public EOrderStatus GetOrderStatus()
    {
        if(RemainingTime >= 0.0f)
        {
            return EOrderStatus.VALID;
        }
        else if(RemainingTime < 0.0f && RemainingTime >= -5.0f)
        {
            return EOrderStatus.LATE;
        }
        else
        {
            return EOrderStatus.REALLY_LATE;
        }
    }

    public float GetOrderTimeLeftProgress()
    {
        return RemainingTime / InitialTime;
    }
}
