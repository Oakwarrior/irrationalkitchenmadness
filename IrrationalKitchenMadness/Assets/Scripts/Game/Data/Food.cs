﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Food
{
    public string Name = "";
    public int Price = 1;
    public Sprite FoodIcon;
    public FoodItem FoodItemTemplate;
}