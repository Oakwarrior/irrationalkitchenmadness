﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Interactable
{

    Vector3 GetPosition();
    void Interact(CharacterBase interactor);
}
